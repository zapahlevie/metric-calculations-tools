package com.tool.calculation.metrics.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Builder
@ToString(exclude = "content")
@EqualsAndHashCode(exclude = "content")
public class FileModel {
    private String path;
    private String filename;
    private String content;
}
