package com.tool.calculation.metrics;

import com.tool.calculation.metrics.service.Main;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;
import java.util.Scanner;

@SpringBootApplication
public class MainApplication {

    public static void main(String[] args) {
        Scanner scan= new Scanner(System.in);
        ConfigurableApplicationContext applicationContext = SpringApplication.run(MainApplication.class, args);
        System.out.println("Input project path : ");
        String path=scan.nextLine();
        Long startTime = System.nanoTime();
        applicationContext.getBean(Main.class).doMetricsCalculation(Arrays.asList(path));
        Long endTime = System.nanoTime();
        printRunTime(startTime, endTime);
        scan.close();
        applicationContext.close();
    }

    private static void printRunTime(Long startTime, Long endTime) {
        double runTime = (double) (endTime - startTime);
        runTime /= 1000000000;
        System.out.println();
        System.out.println("Time consume --> " + runTime + " seconds");
        System.out.println();
    }
}
