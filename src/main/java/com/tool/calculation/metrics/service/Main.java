package com.tool.calculation.metrics.service;

import org.springframework.lang.NonNull;

import java.util.List;

public interface Main {
    void doMetricsCalculation(@NonNull List<String> paths);
}
