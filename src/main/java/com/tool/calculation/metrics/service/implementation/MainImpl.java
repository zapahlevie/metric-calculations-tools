package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.service.FilesDetection;
import com.tool.calculation.metrics.service.Main;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MainImpl implements Main {

    @Autowired
    private FilesDetection filesDetection;

    @Value("${files.mime.type}")
    private String mimeType;

    @Override
    public void doMetricsCalculation(List<String> paths) {
        System.out.println();
        System.out.println("Detecting files...");
        Map<String, List<FileModel>> files = filesDetection.detect(paths, mimeType);
        files.forEach(this::doReadFiles);
    }

    private void doReadFiles(String path, List<FileModel> fileModels) {
        System.out.println("Project Path -> " + path);
        System.out.println("Files detected -> " + fileModels.size());
        fileModels.forEach(this::doPrintFile);
    }

    private void doPrintFile(FileModel fileModel) {
        System.out.println();
        System.out.println("Name : " + fileModel.getFilename());
        System.out.println("Path : " + fileModel.getPath());
//        System.out.println("Content : " + fileModel.getContent());
        System.out.println();
    }
}
